# PostgreSQL Indexing

PostgreSQL indexing playground.

This program has been developed using **Python 3.6** and it's compatible with PostgreSQL 9.x and 10.

## Populate databases
[populate-db.py](populate-db.py) is used to create and populate PostgreSQL databases using random strings.

```
usage: populate-pg-db.py [-h] [--db-count DB_COUNT]
                         [--db-name-len DB_NAME_LEN]
                         [--table-count TABLE_COUNT]
                         [--table-name-len TABLE_NAME_LEN]
                         [--row-count ROW_COUNT] [--string-len STRING_LEN]
                         [--pg-host PG_HOST] [--pg-user PG_USER] [-d]

Create and populate PostgreSQL databases

optional arguments:
  -h, --help            show this help message and exit
  --db-count DB_COUNT   number of databases (default: 5)
  --db-name-len DB_NAME_LEN
                        database name length (default: 20)
  --table-count TABLE_COUNT
                        number of tables (default: 4)
  --table-name-len TABLE_NAME_LEN
                        table name length (default: 10)
  --row-count ROW_COUNT
                        number of rows (default: 500)
  --string-len STRING_LEN
                        string name length (default: 8)
  --pg-host PG_HOST     PostgreSQL host (default: 127.0.0.1)
  --pg-user PG_USER     PostgreSQL user (default: postgres)
  -d, --debug           print debugging statements (default: 20)
```

## Create indexes
[create-indexes.py](create-indexes.py) iterates through every table of every database of a given PostgreSQL server creating a new simple index based on the column `name` and showing the 90 percentile and the max time to create every index.

```
usage: create-indexes.py [-h] [--pg-host PG_HOST] [--pg-user PG_USER]
                      [--percentile PERCENTILE] [-d] [-v]

Iterates through every table of every databases of a given PostgreSQL server
and creates a new simple index based on the column 'name'

optional arguments:
  -h, --help            show this help message and exit
  --pg-host PG_HOST     PostgreSQL host (default: 127.0.0.1)
  --pg-user PG_USER     PostgreSQL user (default: postgres)
  --percentile PERCENTILE
                        percentile (default: 90)
  -d, --debug           print debugging statements (default: 30)
  -v, --verbose         be verbose (default: None)
```

## Setup and usage

```
git clone https://gitlab.com/garcorain/postgres-indexing.git
```

Create Python virtual environment and install dependencies
```
cd postgres-indexing
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
```

Start a PostgreSQL instance
```
docker run -d --name postgres -p 5432:5432 postgres:10
```

Populate databases and run the index creation script
```
./populate-db.py
./create-indexes.py
```
