#!/usr/bin/env python3

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from string import ascii_lowercase, digits
from random import choices
import argparse
import logging
import threading
from time import time

start_time = time()

description = """
    Create and populate PostgreSQL databases
"""

parser = argparse.ArgumentParser(
    description=description,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

parser.add_argument(
    "--db-count",
    type=int,
    help="number of databases",
    default=5
)

parser.add_argument(
    "--db-name-len",
    type=int,
    help="database name length",
    default=20
)

parser.add_argument(
    "--table-count",
    type=int,
    help="number of tables",
    default=4
)

parser.add_argument(
    "--table-name-len",
    type=int,
    help="table name length",
    default=10
)

parser.add_argument(
    "--row-count",
    type=int,
    help="number of rows",
    default=500
)

parser.add_argument(
    "--string-len",
    type=int,
    help="string name length",
    default=8
)

parser.add_argument(
    "--pg-host",
    type=str,
    help="PostgreSQL host",
    default="127.0.0.1"
)

parser.add_argument(
    "--pg-user",
    type=str,
    help="PostgreSQL user",
    default="postgres"
)

parser.add_argument(
    '-d', '--debug',
    help="print debugging statements",
    action="store_const", dest="loglevel", const=logging.DEBUG,
    default=logging.INFO,
)

args = parser.parse_args()

DB_COUNT = args.db_count
DB_NAME_LEN = args.db_name_len
TABLE_COUNT = args.table_count
TABLE_NAME_LEN = args.table_name_len
ROW_COUNT = args.row_count
STRING_LEN = args.string_len
PG_HOST = args.pg_host
PG_USER = args.pg_user

LOG_FORMAT = "%(asctime)s %(levelname)s %(threadName)s %(message)s"

logging.basicConfig(level=args.loglevel, format=LOG_FORMAT)
log = logging.getLogger("populatedb")


def generate_string(length):
    return ''.join(choices(ascii_lowercase + digits, k=length))


def create_db(db_name):
    log.info("creating database {}".format(db_name))

    conn = psycopg2.connect("host='{}' dbname='postgres' user='{}'".format(PG_HOST, PG_USER))
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    cursor.execute("CREATE DATABASE {}".format(db_name))

    conn.close()


def populate_db(db_name):
    log.info("populating database {}".format(db_name))

    conn = psycopg2.connect("host='{}' dbname='{}' user='{}'".format(PG_HOST, db_name, PG_USER))
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    for i in range(0, TABLE_COUNT):
        table_name = 't' + generate_string(TABLE_NAME_LEN)
        log.info("creating table {}".format(table_name))
        cursor.execute("CREATE TABLE {} (id serial, name varchar({}) not null)".format(table_name, STRING_LEN))
        for j in range(0, ROW_COUNT):
            cursor.execute("INSERT INTO {} (name) VALUES ('{}')".format(table_name, generate_string(STRING_LEN)))

    conn.close()


def create_and_populate_db(db_name):
    create_db(db_name)
    populate_db(db_name)


threads = []

for i in range(0, DB_COUNT):
    t = threading.Thread(target=create_and_populate_db, args=("d" + generate_string(DB_NAME_LEN),))
    threads.append(t)

for t in threads:
    t.start()

for t in threads:
    t.join()
    log.debug("{} exited OK".format(t.getName()))

log.debug("duration: {}s".format(time() - start_time))
