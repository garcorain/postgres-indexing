#!/usr/bin/env python3

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.pool import ThreadedConnectionPool
import argparse
import threading
import logging
from time import time
import numpy as np


description = """
    Iterates through every table of every databases of a given PostgreSQL
    server and creates a new simple index based on the column 'name'
"""

parser = argparse.ArgumentParser(
    description=description,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

parser.add_argument(
    "--pg-host",
    type=str,
    help="PostgreSQL host",
    default="127.0.0.1"
)

parser.add_argument(
    "--pg-user",
    type=str,
    help="PostgreSQL user",
    default="postgres"
)

parser.add_argument(
    "--percentile",
    type=int,
    help="percentile",
    default=90
)

parser.add_argument(
    '-d', '--debug',
    help="print debugging statements",
    action="store_const", dest="loglevel", const=logging.DEBUG,
    default=logging.WARNING
)

parser.add_argument(
    '-v', '--verbose',
    help="be verbose",
    action="store_const", dest="loglevel", const=logging.INFO,
)

args = parser.parse_args()

PG_HOST = args.pg_host
PG_USER = args.pg_user
PERCENTILE = args.percentile

LOG_FORMAT = "%(asctime)s %(levelname)s %(threadName)s %(message)s"

logging.basicConfig(level=args.loglevel, format=LOG_FORMAT)
log = logging.getLogger("create-pg-indexes")

index_creation_times = []


def database_list():
    try:
        conn = psycopg2.connect("host='{}' user='{}'".format(PG_HOST, PG_USER))
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = conn.cursor()
        cursor.execute("SELECT datname FROM pg_database")
        exclusion_list = ['template0', 'template1', 'postgres']
        databases = [d[0] for d in cursor.fetchall() if d[0] not in exclusion_list]
        conn.close()
    except psycopg2.Error as err:
        log.error("database_list error: {}".format(err))
        if "conn" in locals():
            conn.close()
        exit(1)

    return databases


def table_list(db_name):
    try:
        conn = psycopg2.connect("host='{}' dbname='{}' user='{}'".format(PG_HOST, db_name, PG_USER))
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = conn.cursor()
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'")
        tables = [t[0] for t in cursor.fetchall()]
        conn.close()
    except psycopg2.Error as err:
        log.error("table_list error: {}".format(err))
        if "conn" in locals():
            conn.close()
        exit(1)

    return tables


def get_max_connections():
    try:
        conn = psycopg2.connect("host='{}' user='{}'".format(PG_HOST, PG_USER))
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = conn.cursor()
        cursor.execute("SHOW max_connections")
        max_conn = int(cursor.fetchall()[0][0])
        conn.close()
    except psycopg2.Error as err:
        log.error("get_max_connections error: {}".format(err))
        if "conn" in locals():
            conn.close()
        exit(1)

    return max_conn


def get_current_connections():
    try:
        conn = psycopg2.connect("host='{}' user='{}'".format(PG_HOST, PG_USER))
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = conn.cursor()
        cursor.execute("SELECT sum(numbackends) FROM pg_stat_database")
        cur_conn = int(cursor.fetchall()[0][0])
        conn.close()
    except psycopg2.Error as err:
        log.error("get_current_connections error: {}".format(err))
        if "conn" in locals():
            conn.close()
        exit(1)

    return cur_conn


def create_connection_pool(db_name):
    dsn = "host='{}' dbname='{}' user='{}'".format(PG_HOST, db_name, PG_USER)

    db_max_conn = get_max_connections()
    db_cur_conn = get_current_connections()
    num_dbs = len(database_list())

    pool_max_conn = (db_max_conn - db_cur_conn) / num_dbs
    pool_min_conn = pool_max_conn / 2

    pool = ThreadedConnectionPool(pool_min_conn, pool_max_conn, dsn)

    return pool


def create_indexes(db_name, pool):
    for table in table_list(db_name):
        try:
            conn = pool.getconn()
            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cursor = conn.cursor()
            log.info("creating new index on table {}".format(table))
            start_time = time()
            cursor.execute("DROP INDEX CONCURRENTLY IF EXISTS {}_name_idx".format(table, table))
            cursor.execute("CREATE INDEX CONCURRENTLY {}_name_idx ON {} (name)".format(table, table))
            end_time = time()
            log.info("adding creation time for {}_name_idx".format(table))
            log.debug("{}_name_idx = {}".format(table, end_time - start_time))
            index_creation_times.append(end_time - start_time)
        except psycopg2.Error as err:
            log.error("create_indexes error: {}".format(err))
        finally:
            if "conn" in locals():
                pool.putconn(conn)


threads = []

for db in database_list():
    pool = create_connection_pool(db)
    t = threading.Thread(target=create_indexes, args=(db, pool,))
    threads.append(t)

for t in threads:
    t.start()

for t in threads:
    t.join()
    log.info("{} exited ok".format(t.getName()))

if index_creation_times:
    log.debug("index_creation_times = {}".format(index_creation_times))
    percentile = np.percentile(index_creation_times, PERCENTILE)
    max_time = max(index_creation_times)
    print("p{}={},max={}".format(PERCENTILE, percentile, max_time))
else:
    log.info("no new indexes created")
